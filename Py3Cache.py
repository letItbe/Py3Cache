# -*- coding: utf-8 -*-

"""
Python Implements Level 2 Cache Broker
"""

import sys
import json
import uuid
import configparser
import redis
import pickle
import threading

from beaker.cache import CacheManager
from beaker.util import parse_cache_config_options


def py3cache_empty():
    return None


class Py3Cache (threading.Thread):

    client_id = ''
    cache_mgr = ''
    redis_conn = ''  # Refer to Instance of redis connection
    redis_pubsub = '' # Redis Pub/Sub Channel
    redis_channel = ''
    debug = False

    def __init__(self):
        self.client_id = uuid.uuid1().__str__()
        threading.Thread.__init__(self)
        config = configparser.ConfigParser()
        config.optionxform = str  # 保留配置文件key的大小写设定
        config.read("config.ini")

        self.debug = "true" == config.get("global", "debug")

        opts = config.items('beaker')
        self.cache_mgr = CacheManager(**parse_cache_config_options(dict(opts)))

        redis_host = config.get("redis", "host")
        redis_port = config.get("redis", "port")
        redis_db = config.getint("redis", "db")
        self.redis_channel = config.get("redis", "channel")

        try:
            self.redis_conn = redis.Redis(host=redis_host, port=redis_port, db=redis_db)
            if self.debug:
                rci = self.redis_conn.info()
                print("Connected to Redis", {"Version": rci['redis_version'], "Platform": rci['os']})

            self.redis_pubsub = self.redis_conn.pubsub()
            self.redis_pubsub.subscribe([self.redis_channel])
            self.start()

        except Exception as e:
            print("Initialize Redis Failed:", e)
            sys.exit()

    def run(self):
        for item in self.redis_pubsub.listen():
            # print(item['data'].__class__)
            if isinstance(item['data'], bytes):
                channel = item['channel'].decode()
                msg = json.loads(item['data'].decode())
                if msg['cid'] != self.client_id:
                    if self.debug:
                        print("[{0}] Message Received: {1}\n".format(channel, msg))

                    cache = self.cache_mgr.get_cache(msg['region'])
                    if msg['action'] == 'clear':
                        cache.clear()
                    elif msg['action'] == 'evict':
                        cache.remove_value(key=msg['key'])
                    elif self.debug:
                        print("Unknown message:", msg)

    def close(self):
        self.redis_pubsub.unsubscribe()

    """ Get Data From Py3Cache """
    def get(self, region, key, createfunc=py3cache_empty):
        cache = self.cache_mgr.get_cache(region)
        val = cache.get(key=key, createfunc=createfunc)
        if val:
            return val

        redis_val = self.redis_conn.hget(region, key)
        if redis_val:
            val = pickle.loads(redis_val)

        if self.debug:
            print("Read data from redis [{0},{1}]=>{2}.".format(region, key, val))
        if val:
            cache.put(key, val)
            return val

        return None

    def put(self, region, key, val):
        cache = self.cache_mgr.get_cache(region)
        cache.put(key, val)
        self.redis_conn.hset(region, key, pickle.dumps(val))
        ''' Broadcast evict message to all Py3Cache nodes'''
        self.redis_conn.publish(self.redis_channel, self.__action("evict", region, key))

    def evict(self, region, key):
        cache = self.cache_mgr.get_cache(region)
        self.redis_conn.hdel(region, key)
        cache.remove_value(key=key)
        ''' Broadcast evict message to all Py3Cache nodes'''
        self.redis_conn.publish(self.redis_channel, self.__action("evict", region, key))

    def clear(self, region):
        cache = self.cache_mgr.get_cache(region)
        self.redis_conn.delete(region)
        cache.clear()
        ''' Broadcast clear message to all Py3Cache nodes'''
        self.redis_conn.publish(self.redis_channel, self.__action("clear", region))

    def __action(self, action, region, key=None):
        msg = {"action": action, "region": region, "key": key, "cid": self.client_id}
        return json.dumps(msg)


if __name__ == '__main__':
    p3c = Py3Cache()

    while True:
        s_input = input("> ")
        if s_input == 'quit' or s_input == 'exit':
            break
        cmds = s_input.split()
        if cmds[0] == 'get':
            data = p3c.get(cmds[1], cmds[2])
            print('[{0},{1}] => {2}'.format(cmds[1], cmds[2], data))
        elif cmds[0] == 'set':
            p3c.put(cmds[1], cmds[2], cmds[3])
            print('[{0},{1}] <= {2}'.format(cmds[1], cmds[2], cmds[3]))
        elif cmds[0] == 'evict':
            p3c.evict(cmds[1], cmds[2])
            print('[{0},{1}] <= None'.format(cmds[1], cmds[2]))
        elif cmds[0] == 'clear':
            p3c.clear(cmds[1])
            print('[{0},*] <= None'.format(cmds[1]))
        else:
            print('Unknown command:', s_input)

    p3c.close()
    sys.exit(0)
