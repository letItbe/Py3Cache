## Py3Cache

### 简介

该项目是我在学习 Python 的时候，为了避免陷入花很长时间去看完一本编程语言学习书后仍然找不到北的状况。针对自己熟悉的一个小领域，换另外一种新语言实现，在实现中有针对性的学习新的语言。

[Py3Cache](https://git.oschina.net/ld/Py3Cache) 是 [J2Cache](https://git.oschina.net/ld/J2Cache) 两级缓存框架的 Python 语言移植版本。关于两级缓存框架的思路已经要解决的问题请看 [J2Cache](https://git.oschina.net/ld/J2Cache) 项目首页中的文章以及视频，这里不再赘述。

Py3Cache 使用 Redis 的 Pub/Sub 进行缓存事件分发。目前的功能还是缺失的，主要是 Beaker 这个一级缓存中的对象在过期失效时候没有通知其他的节点。这个需要再深入研究 Beaker 是否支持类似 Ehcache 的缓存事件通知接口。欢迎有经验者给我一些指点。

### TODO

* 实现当 Beaker 中的数据因为过期失效时通知其他的节点清除一级缓存的数据。

### 环境要求

* Python 3
* Beaker （一级缓存）
* PyRedis （二级缓存）
* Pickle （序列化器）
* Redis （服务）

### 使用方法

1. 安装 Redis 服务并启动
2. 修改 config.ini 中关于 Redis 服务的主机和端口的配置
3. 在 config.ini 的 [beaker] 定义一级缓存，具体请看 config.ini 的示例配置（可选）
4. 启动多个测试应用 : python3 Py3Cache.py

### 测试方法

启动测试应用后可以使用如下命令进行缓存的操作

> exit/quit 退出测试应用
> get [region] [key] 读取 [region] 中的 [key] 缓存数据
> set [region] [key] [val] 设置 [region] 中的 [key] 值为 [val]
> evict [region] [key] 清除某个缓存数据
> clear [region] 清除整段缓存数据

### 寻求帮助

如果你有什么的建议以及完善的需求，请提交 Issue 或者直接提交 Pull Requests。